//#1

db.users.find({
	$or: [
	{firstname: {$regex: 's', $options:'$i'}},
	{lastName: {$regex: 'd', $options:'$i'}}
	]
},
{firstName: 1, lastName: 1, _id: 0}
)


/*2. 	// Find users who are from the HR department 
		// and their age is greater then or equal to 70.*/
    
db.users.find({
	$and: [ 
	{department: {$regex: 'HR', $options:'$i'}},
	{age: {$gte:70}}
	]
},
{firstName: 1, lastName: 1, _id: 0}
)


/*3. // Find users with the letter 'e' in their 
	//first name and has an age of less than or equal to 30.*/
db.users.find({
	$and: [ 
	{firstName: {$regex: 'e', $options:'$i'}},
	{age: {$lte:30}}
	]
},
{firstName: 1, lastName: 1, _id: 0}
)




